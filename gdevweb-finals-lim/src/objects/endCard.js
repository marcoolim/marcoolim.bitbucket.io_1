class EndCard extends Phaser.Group {
    constructor(game) {
        super(game);

        this.init ();

        this.visible = false;
    }

    init () {
        this.game.load.image('logo', 'assets/images/logo.png');

        

        // end card frame
        var width = game.baseResolution.max * 0.5;
        var height = game.baseResolution.max * 0.5;
        this.frame = this.add (this.game.add.graphics());
        this.frame.beginFill (0x000066);
        this.frame.drawRect (-width * 0.5, -height * 0.5, width, height);
        this.frame.endFill ();

        // header text
        var style = {
            font: 'Arial',
            fontSize: 32,
            fill: "#ffffff",
            align: "center",
            fontWeight: 600
        };

        var style2 = {
            font: 'Arial',
            fontSize: 32,
            fill: "#ffffff",
            align: "center",
            fontWeight: 600
        }

        
        this.headerText = this.add (this.game.add.text (0, -game.baseResolution.max * 0.20, "", style));
        this.headerText.anchor.setTo (0.5, 0.5);

        // body text
        style.fontWeight = 0;
        style.fontSize = 24;

        
        this.scoreText = this.add (this.game.add.text (0, -game.baseResolution.max * 0.12, "", style2));
        this.scoreText.anchor.setTo (0.5, 0.5);

        this.gameLogo = this.add(this.game.add.image(0,game.baseResolution.max * 0.07,'logo'));
        this.gameLogo.scale.setTo(0.4);
        this.gameLogo.anchor.setTo (0.5, 0.5);


        this.bodyText = this.add (this.game.add.text (0, -game.baseResolution.max * 0.04, game.strings["ENDCARD_BODY"], style));
        this.bodyText.anchor.setTo (0.5, 0.5);   
    

        // install button
        this.installButton = new InstallButton (game);
        this.installButton.position.setTo (0, game.baseResolution.max * 0.20);
        this.addChild (this.installButton);
    }

    show (condition) {
        this.headerText.text = (condition == "win") ? game.strings["ENDCARD_HEADER_WON"] : game.strings["ENDCARD_HEADER_LOST"] + game.current_best_score_text.text;
        this.scoreText.text = this.game.high_score;
        this.visible = true;
    }
}
