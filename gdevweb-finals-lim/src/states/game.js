class Game extends Phaser.State {
    constructor() {
        super();
        this.config = {
            landscape: {
                background: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.5, 
                    angle: 90
                },
                installButton: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.95,
                    scaleX: 1,
                    scaleY: 1.2
                },
                endCard: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.5,
                    scaleX: 1,
                    scaleY: 1
                }
            },
            portrait: {
                background: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.5,
                    angle: 0
                },
                installButton: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.05,
                    scaleX: 1,
                    scaleY: 1
                },
                endCard: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.5,
                    scaleX: 1,
                    scaleY: 1
                }
            }
        }
    }

    preload() {
        game.load.image('ball', 'assets/images/ball.png');
        game.load.image('hoop', 'assets/images/hoop.png');
        game.load.image('side rim', 'assets/images/side_rim.png');
        game.load.image('front rim', 'assets/images/front_rim.png');

        game.load.image('win0', 'assets/images/win0.png');
        game.load.image('win1', 'assets/images/win1.png');
        game.load.image('win2', 'assets/images/win2.png');
        game.load.image('win3', 'assets/images/win3.png');
        game.load.image('win4', 'assets/images/win4.png');
        game.load.image('lose0', 'assets/images/lose0.png');
        game.load.image('lose1', 'assets/images/lose1.png');
        game.load.image('lose2', 'assets/images/lose2.png');
        game.load.image('lose3', 'assets/images/lose3.png');
        game.load.image('lose4', 'assets/images/lose4.png');

        game.load.audio('score', 'assets/audio/score.wav');
        game.load.audio('backboard', 'assets/audio/backboard.wav');
        game.load.audio('whoosh', 'assets/audio/whoosh.wav');
        game.load.audio('fail', 'assets/audio/fail.wav');
        game.load.audio('spawn', 'assets/audio/spawn.wav');

        game.load.image('logo', 'assets/images/logo.png');
    }

    create() {
        // game variables
        this.hoop;
        this.left_rim;
        this.right_rim;
        this.ball;
        this.front_rim;
        this.current_score = 0;
        this.current_score_text;
        this.high_score_text;
        this.current_best_text;

        this.score_sound,
        this.backboard,
        this.whoosh,
        this.fail,
        this.spawn;

        this.moveInTween,
        this.fadeInTween,
        this.moveOutTween,
        this.fadeOutTween,
        this.emoji,
        this.emojiName;

        this.collisionGroup;

        this.location_interval;
        this.isDown = false;
        this.start_location;
        this.end_location;

        // game container (for resizing)
        game.container = game.world.addChild (new Phaser.Group (game));
        game.container.reorientLayout = this.reorientLayout.bind(this);

        // game container
        this.container = new Phaser.Group (game);
        game.container.add (this.container);

        // background
        this.background = this.container.add (this.game.add.graphics());
        this.background.beginFill (0x99ccff);
        this.background.drawRect (-game.baseResolution.min*0.5, -game.baseResolution.max*0.5, game.baseResolution.min, game.baseResolution.max);
        this.background.endFill ();
        this.background.anchor.setTo (0.5, 0.5);

        // install button at top
        this.installButton = new InstallButton (game);
        this.container.addChild (this.installButton);

        

        ////////

        game.physics.startSystem(Phaser.Physics.P2JS);
    
        game.physics.p2.setImpactEvents(true);
    
        game.physics.p2.restitution = 0.63;
        game.physics.p2.gravity.y = 0;
    
        this.collisionGroup = game.physics.p2.createCollisionGroup();
    
        this.score_sound = game.add.audio('score');
        this.backboard = game.add.audio('backboard');
        this.backboard.volume = 0.5;
        this.whoosh = game.add.audio('whoosh');
        this.fail = game.add.audio('fail');
        this.fail.volume = 0.1;
        this.spawn = game.add.audio('spawn');
    
        game.stage.backgroundColor = "#99ccff";
    
        // high_score_text = game.add.text(450, 25, 'High Score\n' + high_score, { font: 'Arial', fontSize: '32px', fill: '#000', align: 'center' });
        this.current_score_text = this.container.add (this.game.add.text(187, 312, '', { font: 'Arial', fontSize: '40px', fill: '#ff9999', align: 'center' })); // 300, 500
        this.current_best_text = this.container.add (this.game.add.text(143, 281, '', { font: 'Arial', fontSize: '20px', fill: '#000000', align: 'center' }));// 230, 450
        this.current_best_score_text = this.container.add (this.game.add.text(187, 312, '', { font: 'Arial', fontSize: '40px', fill: '#999999', align: 'center' })); // 300, 500
    
        this.hoop = this.container.add (this.game.add.sprite(88, 62, 'hoop')); // 141, 100
        this.left_rim = this.container.add (this.game.add.sprite(150, 184, 'side rim')); // 241, 296
        this.right_rim = this.container.add (this.game.add.sprite(249, 184, 'side rim')); // 398, 296
    
        game.physics.p2.enable([ this.left_rim, this.right_rim], false);
    
        this.left_rim.body.setCircle(2.5);
        this.left_rim.body.static = true;
        this.left_rim.body.setCollisionGroup(this.collisionGroup);
        this.left_rim.body.collides([this.collisionGroup]);
    
        this.right_rim.body.setCircle(2.5);
        this.right_rim.body.static = true;
        this.right_rim.body.setCollisionGroup(this.collisionGroup);
        this.right_rim.body.collides([this.collisionGroup]);
    
        this.createBall();
    
        this.cursors = game.input.keyboard.createCursorKeys();
    
        game.input.onDown.add(this.click, this);
        game.input.onUp.add(this.release, this);

        //////
        this.endCard = new EndCard (game);
        
        // update orientation
        adaptGameToOrientation();
        assignOrientationChangeHandlers(); 

        this.gameStart();
        this.game.debug.text((Math.round(game.time.events.duration)/1000,175,80));
        this.game.time.events.add(Phaser.Timer.SECOND * 15, function () {
            this.game.debug.text.visible = false;
        }, this);


        this.isGameOver = false;
    }

    update() {
        if(this.isGameOver) return;
        if (this.ball && this.ball.body.velocity.y > 0) {
            this.front_rim = this.container.add (this.game.add.sprite(148, 182, 'front rim'));
            this.ball.body.collides([this.collisionGroup], this.hitRim, this);
        }
    
        if (this.ball && this.ball.body.velocity.y > 0 && this.ball.body.y > 188 && !this.ball.isBelowHoop) {
            this.ball.isBelowHoop = true;
            this.ball.body.collideWorldBounds = false;
            var rand = Math.floor(Math.random() * 5);
            if (this.ball.body.x > 151 && this.ball.body.x < 249) {
                this.emojiName = "win" + rand;
                this.current_score += 1;
                this.current_score_text.text = this.current_score;
                this.score_sound.play();
            } else {
                this.emojiName = "lose" + rand;
                this.fail.play();
                if (this.current_score > this.game.high_score) {
                    this.game.high_score = this.current_score;
                // 	high_score_text.text = 'High Score\n' + high_score;
                }
                this.current_score = 0;
                this.current_score_text.text = '';
                this.current_best_text.text = 'Current Best';
                this.current_best_score_text.text = this.game.high_score;
            }
            this.emoji = this.container.add (this.game.add.sprite(180, 100, this.emojiName));
            this.emoji.scale.setTo(0.25, 0.25);
            this.moveInTween = game.add.tween(this.emoji).from( { y: 150 }, 500, Phaser.Easing.Elastic.Out, true);
            this.fadeInTween = game.add.tween(this.emoji).from( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true, 0, 0, false);
            //this.fadeOutTween = game.add.tween(this.emoji).to({alpha:0}, 300, Phaser.Easing.Linear.None, true, 450)
            this.moveInTween.onComplete.add(this.tweenOut, this);
        }
    
        if (this.ball && this.ball.body.y > 1200) {
            game.physics.p2.gravity.y = 0;
            this.ball.kill();
            this.createBall();
        }    

        
    }
    
    tweenOut() {
        this.moveOutTween = game.add.tween(this.emoji).to( { y: 50 }, 600, Phaser.Easing.Elastic.In, true);
        this.moveOutTween.onComplete.add(function() { this.emoji.kill(); }, this);
        this.fadeOutTween = game.add.tween(this.emoji).to({alpha:0}, 300, Phaser.Easing.Linear.None, true, 1000)
    }
    
    hitRim() {
    
        this.backboard.play();
    
    }
    
    createBall() {
        var xpos;
        if (this.current_score === 0) {
            xpos = 200;
        } else {
            xpos = 60 + Math.random() * 280;
        }
        this.spawn.play();
        this.ball = this.container.add (this.game.add.sprite(xpos, 547, 'ball'));
        game.add.tween(this.ball.scale).from({x : 0.7, y : 0.7}, 100, Phaser.Easing.Linear.None, true, 0, 0, false);
        game.physics.p2.enable(this.ball, false);
        this.ball.body.setCircle(60); // NOTE: Goes from 60 to 36
        this.ball.launched = false;
        this.ball.isBelowHoop = false;
    
    }
    
    click(pointer) {
    
        var bodies = game.physics.p2.hitTest(pointer.position, [ this.ball.body ]);
        if (bodies.length) {
            this.start_location = [pointer.x, pointer.y];
            this.isDown = true;
            this.location_interval = setInterval(function () {
                this.start_location = [pointer.x, pointer.y];
            }.bind(this), 200);
        }
    
    }
    
    release(pointer) {
    
        if (this.isDown) {
            window.clearInterval(this.location_interval);
            this.isDown = false;
            this.end_location = [pointer.x, pointer.y];
    
            if (this.end_location[1] < this.start_location[1]) {
                var slope = [this.end_location[0] - this.start_location[0], this.end_location[1] - this.start_location[1]];
                var x_traj = -2300 * slope[0] / slope[1];
                this.launch(x_traj);
            }
        }
    
    }
    
    launch(x_traj) {
    
        if (this.ball.launched === false) {
            this.ball.body.setCircle(36);
            this.ball.body.setCollisionGroup(this.collisionGroup);
            this.current_best_text.text = '';
            this.current_best_score_text.text = '';
            this.ball.launched = true;
            game.physics.p2.gravity.y = 3000;
            game.add.tween(this.ball.scale).to({x : 0.6, y : 0.6}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);
            this.ball.body.velocity.x = x_traj;
            this.ball.body.velocity.y = -1750;
            this.ball.body.rotateRight(x_traj / 3);
            this.whoosh.play();
        }
    
    }

    gameStart () {
        // add a delay timer that will trigger game over
        //this.game.debug.text((Math.round(game.time.events.duration)/1000,175,80));
        game.time.events.add(Phaser.Timer.SECOND * 15, function () {
            this.gameOver ("win");
            this.game.debug.text.visible = false;
        }, this);
        this.game.debug.text((Math.round(game.time.events.duration)/1000,175,80));
    }

    gameOver (condition) {
        this.isGameOver = true;
        // hide the install button at the top
        this.installButton.visible = false;

        // create a transparent black background to dim the game
        this.createDimOverlay ();

        // add the end card on top of the game
        this.container.addChild (this.endCard);
        // pass in the condition to the end card to modify the header text
        this.endCard.show (condition);

        this.hoop.visible = false;
        this.left_rim.visible = false;
        this.right_rim.visible = false;
        this.front_rim.visible = false;
        this.ball.visible = false;
    }

    createDimOverlay () {
        this.overlay = this.container.add (this.game.add.graphics());
        this.overlay.beginFill (0x000000, 0.5);
        this.overlay.drawRect (-game.baseResolution.max*0, -game.baseResolution.max*0, game.baseResolution.max, game.baseResolution.max);
        this.overlay.endFill ();
    }

    reorientLayout (orientation) {
        this.orient = orientation;

        this.background.angle = this.config[orientation].background.angle;
        this.background.x = this.config[orientation].background.x;
        this.background.y = this.config[orientation].background.y;

        this.installButton.x = this.config[orientation].installButton.x;
        this.installButton.y = this.config[orientation].installButton.y;
        this.installButton.scale.x = this.config[orientation].installButton.scaleX;
        this.installButton.scale.y = this.config[orientation].installButton.scaleY;

        this.endCard.x = this.config[orientation].endCard.x;
        this.endCard.y = this.config[orientation].endCard.y;
        this.endCard.scale.x = this.config[orientation].endCard.scaleX;
        this.endCard.scale.y = this.config[orientation].endCard.scaleY;
    }
}
